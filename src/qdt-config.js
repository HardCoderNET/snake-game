module.exports = {
    platforms: {
        "*": {
            "source": "base",
            "libs": {
                // please disable libraries you don't need
                "jquery": true,
                "bootstrap_3": false,
                "angular": false,
                "underscore": true,
                "underscore.string": true,
                "mdi": true,
                "babel_polyfill": true,
            },
            "libs_data": {},
            // overwrite this paths in your platform
            "paths": {
                "root": false,
                "assets_root": false,
                "clean_paths": false
            },
            // add here libraries from node_modules
            "assets": [{
                "from": "resources/assets/**/*",
                "to": "assets"
            }],
            // browsersync configurations (ex: ip, port and path)
            "server": false,
            // tasks configs
            "tasks": {
                // disable tasks
                "disabled": {
                    "pug": false,
                    "js": false,
                    "assets": false
                },
                // tasks details, ex: source, destination, minify and etc. 
                "settings": {
                    "js": [{
                        "src": [
                            "angular-1/modules/**/*",
                            "angular-1/components/**/*",
                            "angular-1/controllers/**/*",
                            "angular-1/directives/**/*",
                            "angular-1/providers/**/*",
                            "angular-1/filters/**/*",
                            "app.ts",
                            "app.js"
                        ],
                        "dest": "/",
                        "name": "app.js",
                        "minify": true,
                        "sourcemap": true,
                        "browserify": true
                    }, {
                        "src": [
                            // "raw/**/*.js"
                        ],
                        "dest": "/raw",
                        "path": "/raw",
                        "minify": true,
                        "sourcemap": false,
                        "browserify": false
                    }],
                    "scss": [{
                        "src": "style.scss",
                        "watch": "includes/**/*.scss",
                        "path": "/",
                        "name": "style.min.css",
                        "minify": true
                    }],
                    "pug": [{
                        "path": "/",
                        "src": ["*.pug"],
                        "watch": ["layout/**/*.pug"],
                        "path": "/"
                    }]
                }
            }
        },
        "game-snake": {
            "source": "game-snake",
            "paths": {
                "root": "../dist/game-snake",
                "assets_root": "../dist/game-snake/assets",
                "clean_paths": [
                    "../dist/game-snake"
                ]
            },
            "server": {
                "path": "/",
                "port": 3000
            }
        }
    }
};