let debug = false;

let skins = {
    light: {
        background: 'silver',
        stroke: '#000',
        canvas: 'whitesmoke',
    },
    dark: {
        background: '#1e1e1e',
        stroke: '#444',
        canvas: '#252526',
    }
};

let skin = localStorage.getItem('skin') || 'dark';

document.toggleSkin = () => {
    skin = skin == 'light' ? 'dark' : 'light';
    $('body').attr('skin', skin);
    localStorage.setItem('skin', skin);
};


$(function() {
    $('body').attr('skin', skin);
    var Vector2 = function (x, y, direction) {
        this.x = x || 0;
        this.y = y || 0;
        this.show = true;
        this.direction = direction || null;
        this.clone = function () {
            return new Vector2(this.x, this.y, this.direction);
        };
        this.equals = function(vector) {
            return this.x == vector.x && this.y == vector.y;
        }
    };

    // canvas and context
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');

    // game settings
    var game_speed = 5;
    var vSyncEnabled = 2;
    var turbo = true;
    var turboTimeout;

    // view size and "pixelation"
    var view_pixels = new Vector2(30, 20);
    var view_size = new Vector2();

    // convert game speed
    game_speed = (1000 / game_speed);
   
    var doRotate = () => {
        view_pixels = new Vector2(view_pixels.y, view_pixels.x);
    };

    var rotate = () => {
        if (localStorage.getItem('rotate') == 'rotate') {
            localStorage.removeItem('rotate');
        } else {
            localStorage.setItem('rotate', 'rotate');
        }

        doRotate();
    }

    document.rotate = rotate;

    if (localStorage.getItem('rotate') == 'rotate') {
        doRotate();
    }

    // helper function
    function rand(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    var xOffset = function (val) {
        return ((window.innerWidth - view_size.x) / 2) + val;
    };

    var calcViewSize = function (x, y) {
        var ratio = view_pixels.x / view_pixels.y;
        var new_canvas_size = new Vector2(y * ratio, y);

        new_canvas_size.pixel_size = new Vector2(
            new_canvas_size.x / view_pixels.x,
            new_canvas_size.y / view_pixels.y
        );

        return new_canvas_size;
    };

    var drawPoints = function (points) {
        points.forEach(drawPoint);
    };

    var drawPoint = function(point) {
        if (point.blinkRate && point.blinkCount >= point.blinkRate) {
            point.show = !point.show;
            if (point.show) {
                point.blinkCount = -point.blinkRate;
            } else {
                point.blinkCount = 0;
            }
        } else {
            point.blinkCount++;
        }

        if (point.show) {
            context.beginPath();
            context.rect(
                xOffset(point.x * view_size.pixel_size.x),
                point.y * view_size.pixel_size.y,
                view_size.pixel_size.x,
                view_size.pixel_size.x
            );
            context.fillStyle = snake.state == 'dead' ? 'red' : '#eee';//point.color || 'orange';
            context.lineWidth = window.innerHeight / 300;
            context.strokeStyle = skins[skin].stroke;
            context.fill();
            context.stroke();
            context.closePath();
        }
    }

    var clearCanvas = function () {
        context.clearRect(0, 0, window.innerWidth, window.innerHeight);
    };

    var drawView = function () {
        context.beginPath();
        context.rect(xOffset(0), 0, view_size.x, view_size.y);
        context.fillStyle = skins[skin].canvas;
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = skins[skin].stroke;
        context.fill();
        context.stroke();
        context.closePath();
    };

    var updateCanvasSize = function () {
        canvas.width = window.innerWidth - 60;
        canvas.height = window.innerHeight - 60;

        context.beginPath();
        context.rect(0, 0, window.innerWidth, window.innerHeight);
        context.fillStyle = skins[skin].background;
        context.fill();
        context.closePath();

        // calc new view size
        view_size = calcViewSize(canvas.width, canvas.height);
    };

    var points = [];

    var snake = {
        size: 3,
        headPosition: new Vector2(5, 15),
        state: "alive",
        direction: "sleep",
        history: [],
        moveCompleted: false,
        move: function () {
            let movingVector = new Vector2();

            if (this.state == "dead") {
                return false;
            }

            if (this.direction == 'right') {
                movingVector.x = (this.headPosition.x < view_pixels.x - 1) ? 1 : -(view_pixels.x - 1);

            } else if (this.direction == 'down') {
                movingVector.y = (this.headPosition.y < view_pixels.y - 1) ? 1 : -(view_pixels.y - 1);

            } else if (this.direction == 'left') {
                movingVector.x = (this.headPosition.x > 0) ? -1 : view_pixels.x;

            } else if (this.direction == 'up') {
                movingVector.y = (this.headPosition.y > 0) ? -1 : view_pixels.y;
            }

            if (movingVector.x !== 0 || movingVector.y !== 0) {
                this.addToHistory(new Vector2(
                    this.headPosition.x, 
                    this.headPosition.y, 
                    this.direction
                ));

                this.headPosition.x += movingVector.x;
                this.headPosition.y += movingVector.y;

                if (food.x == this.headPosition.x && food.y == this.headPosition.y) {
                    this.size++;
                    food = false;
                }
            }

            this.moveCompleted = true;
        },
        addToHistory: function (position) {
            this.history.push(position);
        },
        die: function () {
            this.state = 'dead';
        },
        calcTail: function () {
            var size = this.size;
            var target_points = this.history.slice(-size).concat([
                this.headPosition.clone()
            ]);

            this.history = this.history.slice(-size);

            size = (size) - target_points.length;

            if (size > 0) {
                var _target_points = [new Vector2(
                    this.headPosition.x,
                    this.headPosition.y
                )];

                while (size-- > 0) {
                    _target_points.push(new Vector2(
                        _target_points[_target_points.length - 1].x - 1,
                        _target_points[_target_points.length - 1].y,
                        "right"
                    ));
                }

                for (var i = _target_points.length - 1; i > 0; i--) {
                    this.addToHistory(_target_points[i]);
                }

                return this.calcTail();
            }

            target_points = target_points.reverse();

            for (var j = target_points.length - 1; j >= 1; j--) {
                if ((target_points[j].x == target_points[0].x) && (target_points[j].y == target_points[0].y)) {
                    this.die();
                }
            }

            return target_points;
        },
        checkCollision: function (target_points) {
            var collision = true;

            for (var j = target_points.length - 1; j >= 1; j--) {
                if ((target_points[j].x == this.headPosition.x) && (target_points[j].y == this.headPosition.y)) {
                    collision = false;
                    this.die();
                }
            }

            return collision;
        },
        turn: function (direction) {
            snake.direction = direction;
            snake.moveCompleted = false;
        }
    };

    var getPoints = function () {
        return points;
    };

    var clearPoints = function () {
        points = [];
    };

    var createFood = function (wall) {
        let food_check;

        do {
            let food = new Vector2(
                rand(0, view_pixels.x - 1), 
                rand(0, view_pixels.y - 1)
            );

            let food_check = snake.history.concat([
                snake.headPosition.clone()
            ]).concat(wall).filter(item => item.equals(food)).length;
            
            food.blinkRate = 1;
            food.blinkCount = 0;

            if (food_check == 0) {
                return food;
            }
        } while (food_check !== 0);
    };

    var food = false;

    var getWall = function () {
        let hole_x = view_pixels.x / 5;
        let hole_y = view_pixels.y / 5;

        hole_x = hole_x % 2 ? hole_x - 1 : hole_x;
        hole_y = hole_y % 2 ? hole_y - 1 : hole_y;

        let hole_x_start = Math.floor((view_pixels.x - hole_x) / 2);
        let hole_y_start = Math.floor((view_pixels.y - hole_y) / 2);

        var out = [];

        for (var i = view_pixels.x - 1; i >= 0; i--) {
            if (i < hole_x_start || i > (hole_x_start + hole_x)) {
                out.push(new Vector2(i, 0));
                out.push(new Vector2(i, view_pixels.y - 1));
            }
        }

        for (var j = view_pixels.y - 1; j >= 0; j--) {
            if (j < hole_y_start || j > (hole_y_start + hole_y)) {
                out.push(new Vector2(0, j));
                out.push(new Vector2(view_pixels.x - 1, j));
            }
        }

        return out;
    };

    var runLogics = function () {
        snake.move();

        var wall = getWall();
        var points = snake.calcTail();

        if (!snake.checkCollision(wall)) {
            return;
        }

        clearPoints();

        for (var i = points.length - 1; i >= 0; i--) {
            var color = snake.state == 'dead' ?
                'red' : (i === 0 ? 'green' : 'orange');

            addPointToArray(points[i], color);
        }

        for (var j = wall.length - 1; j >= 0; j--) {
            addPointToArray(wall[j], 'brown');
        }

        if (food === false) {
            food = createFood(wall);
        }

        addPointToArray(food, 'green');
    };

    var addPointToArray = function (new_point, color) {
        var count = points.filter((item) => item.equals(new_point));

        if (color)
            new_point.color = color;

        if (count.length === 0)
            points.push(new_point);
    };

    var frameInterval = (1000 / 60) / game_speed;
    var time = ((new Date())).getTime();
    var fpsTime = ((new Date())).getTime();
    var iterationNth = 0;
    var drawFrameTimeout = false;

    var drawFrame = function () {
        if (drawFrameTimeout) {
            clearTimeout(drawFrameTimeout);
        }

        let frameTime = ((new Date())).getTime();

        if ((frameTime) > (time + (turbo ? game_speed / 2 : game_speed))) {
            time = frameTime;
            vSync();
        }

        let updateIn = 0;

        if (vSyncEnabled == 1) {
            updateIn = (1000 / 60);
        } else if (vSyncEnabled == 2) {
            updateIn = (1000 / 30);
        }

        // set draw interval
        drowFps(frameTime);
        drawFrameTimeout = setTimeout(drawFrame, vSyncEnabled ? (1000 / 60) : 0);
    };

    var vSync = function () {
        runLogics();

        // clear old frame
        clearCanvas();

        // draw game view
        updateCanvasSize();

        // draw game view
        drawView();

        drawPoints(getPoints());
    };

    var drowFps = function (frameTime) {
        iterationNth++;

        if ((frameTime / 1000).toFixed(0) != (fpsTime / 1000).toFixed(0)) {
            debug && console.log('fps', iterationNth);
            iterationNth = 0;
            fpsTime = frameTime;
        }
    };

    var initTurbo = () => {
        if (!turboTimeout) {
            turboTimeout = setTimeout(startTurbo, 250);
        }
    };
    
    var startTurbo = () => {
        turbo = true;
    };

    var stopTurbo = () => {
        clearTimeout(turboTimeout);
        turbo = false;
        turboTimeout = false;
    };

    $(document).keydown(function (e) {
        if (!snake.moveCompleted) {
            return;
        }

        switch (e.which) {
            case 37: {
                if (snake.direction != 'right' && snake.direction != 'sleep') {
                    snake.turn('left');
                    initTurbo();
                }
            }; break;
            case 38: {
                if (snake.direction != 'down') {
                    snake.turn('up');
                    initTurbo();
                }
            }; break;
            case 39: {
                if (snake.direction != 'left') {
                    snake.turn('right');
                    initTurbo();
                }
            }; break;
            case 40: {
                if (snake.direction != 'up') {
                    snake.turn('down');
                    initTurbo();
                }
            }; break;
        }
    });

    $(document).keyup(function(e) {
        switch (e.which) {
            case 37: 
            case 38: 
            case 39:
            case 40: { 
                stopTurbo();
            }; break;
        }
    });

    // redraw on window resize
    window.addEventListener('resize', drawFrame, false);

    // draw first frame
    drawFrame();
});